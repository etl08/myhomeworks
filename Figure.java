package HomeWork_4;

public abstract class Figure {
    int x;
    int y;
    double Pi = 3.14;
    int R;
    int r;

    public Figure(int x, int y, double Pi, int R, int r) {
       this.x = x;
       this.y = y;
       this.Pi = Pi;
       this.R = R;
       this.r = r;
    }

    abstract double getPerimeter ();

}
