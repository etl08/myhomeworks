package HomeWork_4;

    public class Square extends Rectangle implements Moveable {
        public Square(int x, int y, double Pi, int R, int r) {
            super(x, y, Pi, R, r);
        }

        public double getPerimeter() {
            return x * y;
        }

        public void move(int x, int y) {
            this.x += x;
            this.y += y;
        }

    }
