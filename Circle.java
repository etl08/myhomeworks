package HomeWork_4;

public class Circle extends Ellips {
    public Circle(int x, int y, double Pi, int R, int r) {
        super(x, y, Pi, R, r);
    }
    public double getPerimeter () {

        return Pi * R * R;

    }

}
