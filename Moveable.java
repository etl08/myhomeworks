package HomeWork_4;

public interface Moveable {
     public void move(int x, int y) {
          this.x += x;
          this.y += y;
