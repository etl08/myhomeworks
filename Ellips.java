package HomeWork_4;

public class Ellips extends Figure {
    public Ellips(int x, int y, double Pi, int R, int r) {
        super(x, y, Pi, R, r); //R - ольший радиус, r - меньший радиус
    }
    public double getPerimeter () {
        return Pi * R * r;
    }
}
